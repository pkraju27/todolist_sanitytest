import logging

import commonUtil
import todoListClient
import unittest
import xmlrunner


class SanityTest(unittest.TestCase):

    def setUp(self):
        """
        Initialized class instance variables to be used in unit test
        :rtype: object
        """
        self.task1 = {"completed": True, "task": "Create Sanity test for to-do List"}
        self.task2 = {"completed": False, "task": "Create Test Report for to-do List"}
        self.errortask = {"completed": False, "task": ""}
        self.taskId = commonUtil.get_task_from_taskList()
        self.tasklist = todoListClient.get_task_list_api()

    def tearDown(self):
        """

        :return:
        """
        self.tasklist = todoListClient.get_task_list_api()

    def test_get_task_list_api(self):
        self.assertIsNotNone(todoListClient.get_task_list_api())

    def test_get_task_api(self):
        if self.tasklist is not None:
            self.assertIsNotNone(todoListClient.get_task_api(task_id=self.taskId))
            with self.assertRaises(TypeError and AttributeError):
                todoListClient.get_task_api(task_id="1234")
        else:
            logging.critical("Task List is empty")

    def test_create_task_api(self):
        newtask_id = todoListClient.create_task_api(task=self.task1)
        self.tasklist = todoListClient.get_task_list_api()
        self.assertTrue(commonUtil.is_task_present(newtask_id, self.tasklist))
        newtask_id = todoListClient.create_task_api(task=self.task2)
        self.tasklist = todoListClient.get_task_list_api()
        self.assertTrue(commonUtil.is_task_present(newtask_id, self.tasklist))
        with self.assertRaises(Exception):
            newtask_id = todoListClient.create_task_api(task=self.errortask)
            self.tasklist = todoListClient.get_task_list_api()
            self.assertT(commonUtil.is_task_present(newtask_id, self.tasklist))

    def test_modify_task_api(self):
        Newtask = {"completed": False, "task": "Modified Task"}
        modifiedTask_id = todoListClient.modify_task_api(task_id=self.taskId, task=Newtask)
        currentTask = todoListClient.get_task_api(modifiedTask_id)
        self.assertEquals(Newtask['task'] , currentTask['task'])

    def test_mark_completed_api(self):
        self.assertTrue(todoListClient.mark_task_completed_api(task_id=self.taskId))

    def test_mark_incomplete_api(self):
        self.assertTrue(todoListClient.mark_task_incomplete_api(task_id=self.taskId))

    def test_delete_task_api(self):
        self.assertTrue(todoListClient.delete_task_api(task_id=self.taskId))

if __name__ == '__main__':
    testRunner = xmlrunner.XMLTestRunner(output="Reports")
    unittest.main(testRunner=testRunner)
