import Api
import commonUtil
import logging

logging.getLogger().setLevel(logging.INFO)
api = Api.ApiRequest()



def get_task_list_api():
    """
    Returns All the TaskId present in to-do list
    :return:
    """
    response = api.call_api(requestType="GET",
                            uri=commonUtil.get_request_url("/tasks"),
                            parameters=None)
    tasks = response.json()
    task_ids = []
    logging.info(tasks)
    for task in tasks:
        task_ids.append(task['id'])
        if not commonUtil.validate_tasks(task):
            logging.error("Incorrect Data")
    if len(task_ids) != len(set(task_ids)):
        logging.critical("Non unique task id found, Incorrect data")
    return task_ids


def get_task_api(task_id):
    """
    Returns Task of a given task_id
    :param task_id:
    :return:
    """
    logging.info("Fetching task_id :" + task_id)
    response = api.call_api(requestType="GET",
                            uri=commonUtil.get_request_url("/tasks/" + task_id),
                            parameters=None)
    task = response.json()
    if not commonUtil.validate_task(task):
        logging.error("Incorrect Data")
    logging.info(task)
    return task


def create_task_api(task):
    """
    Creates a task with given parameters
    :param task:
    :return:
    """
    try:
        if task['completed'] == '' or task['task'] == '':
            raise Api.UnAcceptedValueError
        logging.info("Creating task")
        response = api.call_api(requestType="POST",
                            uri=commonUtil.get_request_url("/tasks"),
                            parameters=task)
    except Api.UnAcceptedValueError as e:
        logging.info(e)
    task = response.json()
    return task['task_id']



def modify_task_api(task_id, task):
    """
    Modifies existing tasks in to-do list
    :param task_id:
    :param task:
    :return:
    """
    logging.info("Modify task")
    response = api.call_api(requestType="PUT",
                            uri=commonUtil.get_request_url("/tasks/" + task_id),
                            parameters=task)
    task = response.json()
    return task['task_id']


def mark_task_completed_api(task_id):
    """
    Marks task as completed and
    checks if the task Completed status is True or not
    :param task_id:
    :return:
    """
    logging.info("Mark task done")
    api.call_api(requestType="POST",
                 uri=commonUtil.get_request_url("/tasks/" + task_id + "/completed"),
                 parameters=None)
    task = get_task_api(task_id)
    return task['completed']


def mark_task_incomplete_api(task_id):
    """
    Marks task as incomplete and
    checks if the task Completed status is False or not
    :param task_id:
    :return:
    """
    logging.info("Mark task incomplete")
    api.call_api(requestType="POST",
                 uri=commonUtil.get_request_url("/tasks/" + task_id + "/incomplete"),
                 parameters=None)
    task = get_task_api(task_id)
    return not task['completed']


def delete_task_api(task_id):
    """
    Deletes task as incomplete and
    check whether task is deleted or not
    :param task_id:
    :return:
    """
    logging.info("Delete Task" + task_id)
    api.call_api(requestType="DELETE",
                 uri=commonUtil.get_request_url("/tasks/" + task_id),
                 parameters=None)
    return not commonUtil.is_task_present(task_id, get_task_list_api())
