import logging
import json
import requests
from requests import ConnectionError
from werkzeug.exceptions import BadRequest, Forbidden, NotFound, MethodNotAllowed, InternalServerError, BadGateway, \
    ServiceUnavailable, GatewayTimeout, HTTPException, NotImplemented, Unauthorized


class ApiRequest:

    def __init__(self):
        """

        :rtype: object
        """
        self.headers = {

            'Content-Type': "application/json",
            'User-Agent': "PythonSanityTest",
            'Accept': "*/*"
        }
    def call_api(self, requestType, uri, parameters):
        """

        :rtype: object
        """
        parameters = json.dumps(parameters)
        try:
            response = requests.request(requestType, uri, data=parameters, headers=self.headers)
            if response.status_code != 200 and response.status_code != 201:
                self.httpResponseCodeErrorHandler(format(response.status_code))
            else:
                return response
        except ConnectionError as e:
            logging.error(e.message)
            exit()

    def httpResponseCodeErrorHandler(self, status_code):
        """
        Function handles the various standard non-200 response code
        :param status_code:
        :return:
        """
        try:
            if status_code == "400":
                raise BadRequest
            elif status_code == "401":
                raise Unauthorized
            elif status_code == "403":
                raise Forbidden
            elif status_code == "404":
                raise NotFound
            elif status_code == "405":
                raise MethodNotAllowed
            elif status_code == "500":
                raise InternalServerError
            elif status_code == "501":
                raise NotImplemented
            elif status_code == "502":
                raise BadGateway
            elif status_code == "503":
                raise ServiceUnavailable
            elif status_code == "504":
                raise GatewayTimeout
            else:
                raise HTTPException
        except BadRequest:
            logging.error("Bad Api Request")
        except Unauthorized and Forbidden:
            logging.error("Unauthorized or Forbidden access")
        except NotFound:
            logging.error("Request not found")
        except InternalServerError and ServiceUnavailable:
            logging.error("Internal server or Service unavailable error")
        except BadGateway and GatewayTimeout:
            logging.error("Gateway doesn't exist or invalid")
        except:
            logging.error("Something went wrong try again later")

class UnAcceptedValueError(Exception):
    def __init__(self, data):
        self.data = data
    def __str__(self):
        return repr(self.data)