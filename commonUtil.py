import ConfigParser
import logging
import random
import todoListClient


class ConfigPropertyReader:
    config = ConfigParser.RawConfigParser()
    filePath = 'config.ini'

    def __init__(self):
        """

        :rtype: object
        """
        self.config.read(self.filePath)

    def get_property(self, section, property):
        """
        Method to read given config property of a given section
        from config.ini file
        :param section:
        :param property:
        :return:
        """
        return self.config.get(section, property)

    def get_base_url(self):
        """
        Returns Base url along with port as configured
        in config.ini file
        :return:
        """
        hostname = self.get_property('host-config', 'hostname')
        port = self.get_property('host-config', 'port')
        return hostname + ':' + port

def get_request_url(uri):
    """
    Accepts Uri argument to form request URL
    :param uri:
    :return:
    """
    property = ConfigPropertyReader()
    return "http://" + property.get_base_url() + uri

def check_response_code(responseCode):
    """

    :param responseCode:
    :return:
    """
    if responseCode != 200 and responseCode != 201:
        print responseCode
        logging.error("Non 200 status code")
        return False
    else:
        logging.info("Api request passed")
        return True

def validate_tasks(task):
    isTaskCompleted = task['completed']
    taskId = task['id']
    taskDescription = task['task']
    if isTaskCompleted != True and isTaskCompleted != False:
        logging.critical("Completed flag is boolean should be true or false")
        return False
    if taskDescription == '' or taskId == '':
        logging.error("Task or id can't be empty")
        return False
    return True


def validate_task(task):
    isTaskCompleted = task['completed']
    taskDescription = task['task']
    if isTaskCompleted != True and isTaskCompleted != False:
        logging.critical("Completed flag is boolean should be true or false")
        return False
    if taskDescription == '':
        logging.error("Task or id can't be empty")
        return False
    return True


def is_task_present(task_id, task_list):
    for task in task_list:
        if task_id == task:
            return True
        else:
            continue
    return False

def get_task_from_taskList():
    tasks = todoListClient.get_task_list_api()
    try:
        task = random.choice(tasks)
        return task
    except IndexError:
        return None









